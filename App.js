import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import UserList from "./src/userList";
import UserDetails from "./src/userDetails";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Users">
        <Stack.Screen name="Users" component={UserList} />
        <Stack.Screen name="DetalhesDosUsuarios" component={UserDetails} options={{title:'Informações dos Usuários'}}/>
      </Stack.Navigator>
    </NavigationContainer>
  );

 
}

