import React, { useEffect, useState } from "react";
import { View, Text, TouchableOpacity, FlatList } from "react-native";
import axios from "axios";

const UserList = ({ navigation }) => {
    const [users, setUsers] = useState([]);
    useEffect(()=>{
        getUsers()
    },[]);
    
    async function getUsers(){
        try{
            const response = await axios.get('https://jsonplaceholder.typicode.com/users')
        setUsers(response.data);
        }catch(error){
            console.error(error)
        }
    }
    
     const UserPress=(user)=>{
         navigation.navigate('DetalhesDosUsuarios',{user})

    }
  return (
    <View>
      <FlatList 
      data={users}
      keyExtractor={(item)=> item.id.toString}
      renderItem={({item})=>(
        <TouchableOpacity onPress={() => UserPress(item)}>
            <Text>{item.name}</Text>
        </TouchableOpacity>
      )}
      />
    </View>
  );
};
export default UserList;
