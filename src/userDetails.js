import React, { useEffect, useState } from "react";
import { View, Text, TouchableOpacity, FlatList } from "react-native";
import axios from "axios";

const UserDetails = ({ route }) => {
   
    const {user} = route.params;

    return(
        <View>
            <Text>Detalhes dos Usuários: </Text>
            <Text>Nome: {user.name}</Text>
            <Text>Username: {user.username}</Text>
            <Text>Telefone: {user.phone}</Text>
            <Text>Email: {user.email}</Text>
            <Text>Endereço:</Text>
            <Text>Cidade - {user.address.city}</Text>
            <Text>Rua - {user.address.street}</Text>
            <Text>Suíte - {user.address.suite}</Text>
            <Text>CEP - {user.address.zipcode}</Text>
            <Text>Empresa: {user.company.name} </Text>
    
            
        </View>
    )

}
export default UserDetails;
